import React from "react";
import { useParams } from "react-router-dom";

import data from "./countries/countries_data";

function Details() {

  let { countryCode } = useParams();

  return(
    <div>
      <div class="container-sm">
        <div class="row">
          <div class="col-sm">
          <h1>Place: {countryCode}</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-sm">
            <div class="text" dangerouslySetInnerHTML={{ __html: getInfo(countryCode) }}></div>
          </div>
        </div>
      </div>
    </div>
  );
}


function getInfo(code) {

  var result = '';

  data.map((item, key) => {

    console.log('item', item);
    var c = item.code;

    if (c == code) {
      result = item.text;
    }
  });

  console.log('result', result);

  let formatted = convertFormat(result);

  return formatted;
}

function convertFormat(string) {
  return string.replace(/(\W|^)_([^_]*)_(\W|$)/g, "$1<em>$2</em>$3").replace(/(\W|^)\*([^*]*)\*(\W|$)/g, "$1<strong>$2</strong>$3")
}


export default Details;