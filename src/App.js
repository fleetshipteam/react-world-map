import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import MapParentComp from './components/MapParentComp';
import China from './components/China';
import Editor from './components/Editor';


function App() {

  return (
    <Router>
      <div className="App">
        
        <Route exact path="/">
          <header className="App-header">
          </header>

          <MapParentComp />

        </Route>

        <Route path="/china">
          <header className="App-header">
            <China />
          </header>
        </Route>
        
        <Route path="/editor">
          <Editor />
        </Route>

      </div>
    </Router>
  );
}

export default App;
