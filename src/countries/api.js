

// request to API 
function getCountriesInformationFromAPI() {

  const axios = require('axios');
  var url = "https://www.thinglink.com/api/media/1281154382271873026";

  axios.get(url)
    .then(function (response) {
      // handle success
      var countriesData = response.data.results.tags;
      console.log('Found countries data', countriesData.length);

      if (countriesData) {
        var cleanedData = getCleanData(countriesData);
        console.log('Countries after cleaning', cleanedData.length);
        var sortedData = getSortedCountries(cleanedData);
        var countries = setCountryCodes(sortedData);
        saveJsonLocally('countries_data', countries);
      } else {
        console.log('Can\'t find countries data');
      }
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
}

function getChinaInformationFromAPI() {

  const axios = require('axios');
  var url = "https://www.thinglink.com/api/media/1281454909589291009";

  axios.get(url).then(function (response) {

    var countriesData = response.data.results.tags;
    console.log('Found countries data', countriesData.length);

    if (countriesData) {
      var cleanedData = getCleanData(countriesData);
      console.log('Countries after cleaning', cleanedData.length);
      var sortedData = getSortedCountries(cleanedData);
      var countries = setCountryCodes(sortedData);
      saveJsonLocally('china_data', countries);
    } else {
      console.log('Can\'t find countries data');
    }
    
  })
  .catch(function (error) {
    console.log(error);
  });
}

function getAfricaInformationFromAPI() {

  const axios = require('axios');
  var url = "https://www.thinglink.com/api/media/1295762492210806785";

  axios.get(url).then(function (response) {

    var countriesData = response.data.results.tags;
    console.log('Found countries data', countriesData.length);

    if (countriesData) {
      var cleanedData = getCleanData(countriesData);
      console.log('Countries after cleaning', cleanedData.length);
      var sortedData = getSortedCountries(cleanedData);
      var countries = setCountryCodes(sortedData);
      saveJsonLocally('africa_data', countries);
    } else {
      console.log('Can\'t find countries data');
    }
    
  })
  .catch(function (error) {
    console.log(error);
  });
}

function getCaribbeanInformationFromAPI() {

  const axios = require('axios');
  var url = "https://www.thinglink.com/api/media/1296755202921070593";

  axios.get(url).then(function (response) {

    var countriesData = response.data.results.tags;
    console.log('Found countries data', countriesData.length);

    if (countriesData) {
      var cleanedData = getCleanData(countriesData);
      console.log('Countries after cleaning', cleanedData.length);
      var sortedData = getSortedCountries(cleanedData);
      var countries = setCountryCodes(sortedData);
      saveJsonLocally('caribbean_data', countries);
    } else {
      console.log('Can\'t find countries data');
    }
    
  })
  .catch(function (error) {
    console.log(error);
  });
}



// Clear data
function getCleanData(array) {
  
  var result = [];
  
  for (item of array) {
  
    var data = item.data;
  
    if (data) {
      result.push(data);
    }
  }
  return result;
}



// Sort countries by alphabet
function getSortedCountries(array) {

  array.sort(function (a, b) {
    if (a.title > b.title) {
      return 1;
    }
    if (a.title < b.title) {
      return -1;
    }
    return 0;
  });

  return array;
}



// Set country codes by title
function setCountryCodes(countries) {

  var iso = require('./iso.json');

  for (country of countries) {
  
    var title = country.title;
  
    if (title) {

      var t = title.replace("Ports", "").replace("Port", "").trim();

      var code = iso[t.trim()];
      if (code) {
        country['code'] = code;
      }
    }
  }

  return countries;
}



// Save data to local file
function saveJsonLocally(file, json) {
  
  var fs = require('fs');
  var string = JSON.stringify(json);
  var filename = file + '.json';

  fs.writeFile(filename, string, function(err, result) {
    if (err) {
      console.log('Error while saving countris data to file', filename, err);
    } else {
      console.log('Countries data saved to file', filename);
    }
  });
}



// Provide country info by country code
function getCountryInfoByCode(code) {
 
  var countries = require('./countries_data.json');

  for (country of countries) {
   
    var c = country['code'];

    if (c == code) {
      return country;
    }
  }

  return null;
}



// Provide country info by country code
function getCountryInfoByCodeFromLocalFile(code) {
 
  var countries = require('./countries_data.json');

  for (country of countries) {
   
    var c = country['code'];

    if (c == code) {
      return country;
    }
  }

  return null;
}




function printData() {

  var items = require('./china_data.json');

  for (item of items) {
    console.log(item.code, ' -- ',item.title)
  }
}




// getCountriesInformationFromAPI();
// getChinaInformationFromAPI();
// getAfricaInformationFromAPI();
// getCaribbeanInformationFromAPI();

// setCountryCodes();

// var c = getCountryInfoByCode('HK');
// console.log(c);

// printData();