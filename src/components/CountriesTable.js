import React, { useEffect } from 'react';
import { MDBDataTable } from 'mdbreact';
import DetailsModal from './DetailsModal';

const CountriesTable = ({data}) => {
  
  const [tableData, setTableData] = React.useState(false);
  const [modalShow, setModalShow] = React.useState(false);
  const [selectedCountry, setSelectedCountry] = React.useState(null);

  var metadata = {
    columns: [
      {
        label: 'Name',
        field: 'name',
        sort: 'asc',
        width: 200
      }
    ],
    rows: [
      {
        name: 'Demo'
      }
    ]
  };

  var countries = [];
  var africa = [];
  var caribbean = [];
  var middleeast = [];
  var china = [];

  useEffect(() => {
    prepareData(data);
  }, [data]);

  return (
    <div className="Countries-Table col-10 col-sm-8 col-md-6 col-lg-5 col-xl-4">
      <MDBDataTable
        striped
        bordered
        hover
        paging={false}
        entries={500}
        data={tableData}
      />
      <DetailsModal show={modalShow} country={selectedCountry} onHide={() => setModalShow(false)} />
    </div>
  );

  function prepareData(data) {

    if (data === undefined) { return }

    if (data.global) {
      countries = JSON.parse(data.global);
    }

    if (data.africa) {
      africa = JSON.parse(data.africa);
    }

    if (data.caribbean) {
      caribbean = JSON.parse(data.caribbean);
    }

    if (data.middleeast) {
      middleeast = JSON.parse(data.middleeast);
    }

    if (data.china) {
      china = JSON.parse(data.china);
    }

    var countriesData = [];

    countries.forEach(element => {
      var title = element.title;
      if (title) {
        countriesData.push({
          "name": title,
          "element": element,
          "clickEvent": () => handleClick(element)
        });
      }
    });

    china.forEach(element => {
      var title = element.title;
      if (title) {
        countriesData.push({
          "name": title,
          "element": element,
          "clickEvent": () => handleClick(element)
        });
      }
    });

    africa.forEach(element => {
      var title = element.title;
      if (title) {
        countriesData.push({
          "name": title,
          "element": element,
          "clickEvent": () => handleClick(element)
        });
      }
    });

    caribbean.forEach(element => {
      var title = element.title;
      if (title) {
        countriesData.push({
          "name": title,
          "element": element,
          "clickEvent": () => handleClick(element)
        });
      }
    });

    // sort
    countriesData.sort(function (a, b) {
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
      return 0;
    });

    // remove duplacates
    var array = removeDuplicates(countriesData);

    metadata.rows = array;

    setTableData(metadata);
  }

  function handleClick(item) {
    showModalView(item);
  }

  function removeDuplicates(array) {

    var names = [];
    var result = [];

    array.forEach(function(item) {

      const name = item.name;

      if (name) {

        if(names.indexOf(name) < 0) {
          names.push(name);
          result.push(item);
        }
      } 
    });

    return result;
  }

  function showModalView(item) {

    let countryCode = item.code;
    console.log('## showModalView countryCode', countryCode);

    if (countryCode) {
      
      if (countryCode === 'CN') {
        let path = '/china';
        window.location.assign(path);
        return;
      }
  
      var g = getCountryByCode(countries, countryCode);
      if (g) {
        setSelectedCountry(g);
        setModalShow(true);
        return;
      }
  
      var a = getCountryByCode(africa, countryCode);
      if (a) {
        setSelectedCountry(a);
        setModalShow(true);
        return;
      }
  
      var c = getCountryByCode(caribbean, countryCode);
      if (c) {
        setSelectedCountry(c);
        setModalShow(true);
        return;
      }

      var m = getCountryByCode(middleeast, countryCode);
      if (m) {
        setSelectedCountry(m);
        setModalShow(true);
        return;
      }
  
      var place = getChinaPlace(countryCode);
      if (place) {
        setSelectedCountry(place);
        setModalShow(true);
        return;
      }
    }


    let title = item.title;
    console.log('## showModalView country title', title);

    if (title) {

      var gt = getCountryByTitle(countries, title);
      if (gt) {
        setSelectedCountry(gt);
        setModalShow(true);
        return;
      }
  
      var at = getCountryByTitle(africa, title);
      if (at) {
        setSelectedCountry(at);
        setModalShow(true);
        return;
      }
  
      var ct = getCountryByTitle(caribbean, title);
      if (ct) {
        setSelectedCountry(ct);
        setModalShow(true);
        return;
      }

      var mt = getCountryByTitle(middleeast, title);
      if (mt) {
        setSelectedCountry(mt);
        setModalShow(true);
        return;
      }

      var cht = getCountryByTitle(china, title);
      if (cht) {
        setSelectedCountry(cht);
        setModalShow(true);
        return;
      }
    } 
  }

  function getCountryByCode(array, code) {

    const result = array.find(function(country) {
      return country.code === code;
    });

    return result;
  }

  function getCountryByTitle(array, title) {

    const result = array.find(function(country) {
      return country.title === title;
    });

    return result;
  }

  function getChinaPlace(countryCode) {

    var country = getChinaPlaceByCode(countryCode);
    if (country) {
      return country;
    }

    var place = getChinaPlaceByTitle(countryCode);
    if (place) {
      return place;
    }

    return null;
  }

  function getChinaPlaceByCode(code) {

    const result = china.find(function(country) {
      return country.code === code;
    });

    return result;
  }

  function getChinaPlaceByTitle(title) {

    const result = china.find(function(port) {

      var t = cleaned(title);
      var pcode = cleaned(port.code);
      var ptitle = cleaned(port.title);

      return pcode === t || ptitle === t;
      
    });

    return result;
  }

  function cleaned(string) {

    if (string) {
      return string.replace("Ports", "").replace("Port", "").trim();
    }

    return "";
  }
}

export default CountriesTable;