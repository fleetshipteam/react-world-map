import React, { useEffect } from 'react';
import Button from 'react-bootstrap/Button'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import ButtonToolbar from 'react-bootstrap/ButtonToolbar'
import ReactJson from 'react-json-view'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Toast from 'react-bootstrap/Toast'

const Editor = () => {
  
  const [selectedCountry, setSelectedCountry] = React.useState(undefined);
  const [textOld, setTextOld] = React.useState({'Please': 'Select a country'});
  const [textNew, setTextNew] = React.useState({'Please': 'Select a country'});
  const [dateOld, setDateOld] = React.useState("Newest version");
  const [dateSaved, setDateSaved] = React.useState("Saved version");
  const [savedObject, setSavedObject] = React.useState(undefined);
  const [isSaveButtonActive, setIsSaveButtonActive] = React.useState(false);

  const [showA, setShowA] = React.useState(false);
  const toggleShowA = () => setShowA(!showA);

  useEffect(() => {

  }, []);

  const goBack = () => {
    window.location.href="/";
  };

  const editFunc = (edit) => {
    console.log('edit', edit);
    setIsSaveButtonActive(true);
    setSavedObject(edit.updated_src);
  };

  const loadCountryData = (country) => {

    console.log('load countries', country);

    if (country === undefined) {
      country = 'global'
    }

    const axios = require('axios');

    // var url = "http://localhost:3000/dev/get-data-for-editor?id=" + country;
    var url = "https://2v2zsydqb5.execute-api.us-east-2.amazonaws.com/dev/get-data-for-editor?id=" + country;

    if (process.env.REACT_APP_BUILD_MODE === 'prod') {
      url = "https://wpyc9euq91.execute-api.us-east-2.amazonaws.com/prod/get-data-for-editor?id=" + country;
    }
  
    axios.get(url).then(function (response) {

      var postfix = '';
      if (process.env.REACT_APP_BUILD_MODE === 'dev') {
        postfix = 'dev'
      }

      var dataArray = response.data;
      let parsedData = dataArray[0];
      let editedData = dataArray[1];

      if (parsedData) {

        let d1 = JSON.parse(parsedData).Item.data;
        let d2 = JSON.parse(editedData).Item.data;

        var parsedText = JSON.parse(d1);
        setTextOld(parsedText);

        let timestampOld = JSON.parse(parsedData).Item.timestamp;
        let formatedOldDate = convertTimestampToDateFormat(timestampOld);
        let oldDateResult = 'Newest version (' + formatedOldDate + ')';
        setDateOld(oldDateResult);

        let timestampSavedDate = JSON.parse(editedData).Item.timestamp;
        let formatedSavedDate = convertTimestampToDateFormat(timestampSavedDate);
        let savedDateResult = 'Saved version (' + formatedSavedDate + ') ' + postfix;
        setDateSaved(savedDateResult);

        if (d2) {
          var editedText = JSON.parse(d2);
          setTextNew(editedText);
        } else {
          setTextNew(parsedText);
        }

      } else {
        console.log("No valid data");
        return;
      }

    }).catch(function (error) {
      console.log('## Error:', error);
    });
  }

  const handleCountryClick = (country) => {
    
    console.log(country);
    
    let loading = {'Please wait': 'Loading... ' + country};

    setTextOld(loading);
    setTextNew(loading);

    setSelectedCountry(country);
    
    loadCountryData(country);
  };

  const handleSaveClick = () => {

    console.log("## save");

    if (selectedCountry === undefined || savedObject === undefined) {
      console.log('Nothing to save');
      return;
    }

    submitEditedData();
  };

  function submitEditedData() {

    (async () => {
      
      setIsSaveButtonActive(false);

      const axios = require('axios');

      // var url = 'http://localhost:3000/dev/edit-country-data';
      var url = "https://2v2zsydqb5.execute-api.us-east-2.amazonaws.com/dev/edit-country-data";

      if (process.env.REACT_APP_BUILD_MODE === 'prod') {
        url = "https://wpyc9euq91.execute-api.us-east-2.amazonaws.com/prod/edit-country-data";
      }

      let id = selectedCountry + '_edited';
      let data = JSON.stringify(savedObject);

      const response = await axios.post(url,
        { data: data, country: id },
        { headers: { 'Content-Type': 'application/json' } }
      )

      setShowA(true);
      setSavedObject(undefined);
      console.log("## submitEditedData done");
      console.log(response.data);
    })();
  }

  function convertTimestampToDateFormat(timestamp) {

    if (timestamp === undefined) {
      return '';
    }

    let unix_timestamp = parseInt(timestamp);

    let date = new Date(unix_timestamp);
    var day = date.toLocaleDateString("en-US");
    var time = date.toLocaleTimeString("en-US");

    return day + ' ' + time;
  }

  return (
    <div className="Editor">
     
      <Container fluid>
        <Row>
          
          <Col></Col>
          
          <Col xs={8}>
            <div className="Editor-ToolBar">
              <ButtonToolbar className="mt-4 mb-3">
                
                <ButtonGroup className="mr-3">
                  <Button variant="secondary" onClick={ goBack } >Back</Button>
                </ButtonGroup>

                <ButtonGroup className="mr-3">
                  <Button variant="light" onClick={ handleCountryClick.bind(null, 'global') } >World</Button>
                  <Button variant="light" onClick={ handleCountryClick.bind(null, 'china') } >China</Button>
                  <Button variant="light" onClick={ handleCountryClick.bind(null, 'africa') } >Africa</Button>
                  <Button variant="light" onClick={ handleCountryClick.bind(null, 'caribbean') } >Caribbean</Button>
                  <Button variant="light" onClick={ handleCountryClick.bind(null, 'middleeast') } >Middle East</Button>
                </ButtonGroup>

                <ButtonGroup>
                  <Button variant="success" disabled={ !isSaveButtonActive } onClick={ handleSaveClick.bind(null, null) } >Save</Button>
                </ButtonGroup>

              </ButtonToolbar>
            </div>
          </Col>

          <Col>
            <Toast show={showA} onClose={toggleShowA} autohide={true} duration={3000}>
              <Toast.Header>
                <strong className="mr-auto">Notification</strong>
              </Toast.Header>
              <Toast.Body>Data saved in a cloud!</Toast.Body>
            </Toast>
          </Col>

        </Row>
      </Container>

      <Container className="Editor-Wrapper">
        <Row>
          <Col>
            <p>{ dateOld }</p>
            <div className="Editor-Text-View Card-Shadow">
              <ReactJson className = "Editor-Text-View" 
                editable = { true } 
                displayDataTypes = { false }
                iconStyle = { "circle" }
                indentWidth = { 2 }
                collapseStringsAfterLength = { 300 }
                src = { textOld } 
              />
            </div>
          </Col>
          <Col>
            <p>{ dateSaved }</p>
            <div className="Editor-Text-View Card-Shadow">
              <ReactJson className = "Editor-Text-View"
                editable = { true } 
                onEdit = { editFunc } 
                displayDataTypes = {false}
                iconStyle = { "circle" }
                indentWidth = { 2 }
                collapseStringsAfterLength = { 300 }
                src = { textNew }
              />
            </div>
          </Col>
        </Row>
      </Container>
      
    </div>
  );
}

export default Editor;