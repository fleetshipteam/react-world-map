import React, { useEffect } from 'react';
import DetailsModal from './DetailsModal';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldLow";
import am4themes_frozen from "@amcharts/amcharts4/themes/frozen";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_frozen);
am4core.useTheme(am4themes_animated);

const MapHook = ({data}) => {

  const [modalShow, setModalShow] = React.useState(false);
  const [selectedCountry, setSelectedCountry] = React.useState(null);

  var countries = [];
  var africa = [];
  var caribbean = [];
  var middleeast = [];

  useEffect(() => {

    // Create map instance
    var chart = am4core.create("chartdiv", am4maps.MapChart);

    chart.maxZoomLevel = 1;
    chart.seriesContainer.draggable = false;
    chart.seriesContainer.resizable = false;

    // Set map definition
    chart.geodata = am4geodata_worldLow;

    // Set projection
    chart.projection = new am4maps.projections.Miller();

    // Create map polygon series
    var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

    // Exclude Antartica
    polygonSeries.exclude = ["AQ"];

    // Make map load polygon (like country names) data from GeoJSON
    polygonSeries.useGeodata = true;

    polygonSeries.tooltip.getFillFromObject = false;
    polygonSeries.tooltip.background.fill = am4core.color("#343440");
    polygonSeries.tooltip.background.stroke = am4core.color("#343440");
    
    // Set Custom Countries data
    var countriesData = [];

    let china = {
      "id": "CN",
      "title": "China",
      "fill": am4core.color("#0091B8"),
      "cursor": am4core.MouseCursorStyle.pointer
    }
    countriesData.push(china);


    polygonSeries.data = countriesData;

    // Configure series
    var polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    polygonTemplate.fill = am4core.color("#E8E8E8");
    polygonTemplate.propertyFields.fill = "fill";
    polygonTemplate.propertyFields.cursorOverStyle = "cursor";

    polygonTemplate.events.on("hit", function(ev) {
      var countryCode = ev.target.dataItem.dataContext.id;
      showModalView(countryCode);
    });

    // Create hover state and set alternative fill color
    var hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color("#8EC6D9"); 

    prepareData((d) => {
      polygonSeries.data = d;
    });

  }, [data])
  
  const prepareData = (callback) => {

    if (data === undefined) { return }

    if (data.global) {
      countries = JSON.parse(data.global);
    }

    if (data.africa) {
      africa = JSON.parse(data.africa);
    }

    if (data.caribbean) {
      caribbean = JSON.parse(data.caribbean);
    }

    if (data.middleeast) {
      middleeast = JSON.parse(data.middleeast);
    }

    var result = [];

    let color = am4core.color("#0091B8");

    countries.forEach(element => {
      let item = {
        "id": element.code,
        "text": element.text,
        "title": element.title,
        "fill": color,
        "cursor": am4core.MouseCursorStyle.pointer
      }
      result.push(item);
    });

    africa.forEach(element => {
      let item = {
        "id": element.code,
        "text": element.text,
        "title": element.title,
        "fill": color,
        "cursor": am4core.MouseCursorStyle.pointer
      }
      result.push(item);
    });

    caribbean.forEach(element => {
      let item = {
        "id": element.code,
        "text": element.text,
        "title": element.title,
        "fill": color,
        "cursor": am4core.MouseCursorStyle.pointer
      }
      result.push(item);
    });

    middleeast.forEach(element => {
      let item = {
        "id": element.code,
        "text": element.text,
        "title": element.title,
        "fill": color,
        "cursor": am4core.MouseCursorStyle.pointer
      }
      result.push(item);
    });

    let china = {
      "id": "CN",
      "title": "China",
      "fill": color,
      "cursor": am4core.MouseCursorStyle.pointer
    }
    result.push(china);

    callback(result);
  }


  return (
    <div className='Map'>
      <div className='Map-Title-Wrapper'>
        <span className='Map-Title'>Fleet Management Limited</span><br></br>
        <span className='Map-Subtitle'>COVID-19 Port Restrictions</span>
      </div>
      <div id="chartdiv" style={{ width: "100%", height: "600px" }}></div>
      <DetailsModal show={modalShow} country={selectedCountry} onHide={() => setModalShow(false)} />
    </div>
  )


  function showModalView(countryCode) {

    if (countryCode === 'CN') {
      let path = '/china';
      window.location.assign(path);
      return;
    }

    var country = getCountryByCode(countryCode);
    if (country) {
      setSelectedCountry(country);
      setModalShow(true);
      return;
    }

    var africanCountry = getAfricanCountryByCode(countryCode);
    if (africanCountry) {
      setSelectedCountry(africanCountry);
      setModalShow(true);
      return;
    }

    var caribbeanCountry = getCaribbeanCountryByCode(countryCode);
    if (caribbeanCountry) {
      setSelectedCountry(caribbeanCountry);
      setModalShow(true);
      return;
    }

    var middleEastCountry = getMiddleEastCountryByCode(countryCode);
    if (middleEastCountry) {
      setSelectedCountry(middleEastCountry);
      setModalShow(true);
      return;
    }
  }

  function getCountryByCode(code) {

    const result = countries.find(function(country) {
      return country.code === code;
    });

    return result;
  }

  function getAfricanCountryByCode(code) {

    const result = africa.find(function(country) {
      return country.code === code;
    });

    return result;
  }
  
  function getCaribbeanCountryByCode(code) {

    const result = caribbean.find(function(country) {
      return country.code === code;
    });

    return result;
  }

  function getMiddleEastCountryByCode(code) {

    const result = middleeast.find(function(country) {
      return country.code === code;
    });

    return result;
  }
}

export default MapHook;