import React from 'react';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

function DetailsModal(props) {
  
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
        COVID-19 Port Restrictions
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>{ getTitle(props) }</h4>
        <div className="row">
          <div className="col-sm">
            <div className="text" dangerouslySetInnerHTML={{ __html: getText(props) }}></div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

function getText(props) {

  if (props) {

    var country = props.country;
    if (country) {

      var text = country.text;
      if (text) {
        return convertFormat(text);
      }
    }
  }

  return '';  
}

function getTitle(props) {

  if (props) {

    var country = props.country;
    if (country) {

      var title = country.title;
      if (title) {
        return title;
      }
    }
  }

  return '';
}


function convertFormat(string) {
  return string.replace(/(\W|^)_([^_]*)_(\W|$)/g, "$1<em>$2</em>$3").replace(/(\W|^)\*([^*]*)\*(\W|$)/g, "$1<strong>$2</strong>$3")
}

export default DetailsModal;