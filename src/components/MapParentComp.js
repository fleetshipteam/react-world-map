import React, { useEffect } from 'react';

import MapHook from './MapHook';
import CountriesTable from './CountriesTable';

const MapParentComp = () => {

  const [data, setData] = React.useState(undefined);

  const loadCountryData = () => {
  
    console.log('## Parent load countries');

    const axios = require('axios');
    // var url = "http://localhost:3000/dev/get-data-for-map"
    var url = "https://2v2zsydqb5.execute-api.us-east-2.amazonaws.com/dev/get-data-for-map";

    if (process.env.REACT_APP_BUILD_MODE === 'prod') {
      url = "https://wpyc9euq91.execute-api.us-east-2.amazonaws.com/prod/get-data-for-map";
    }
  
    axios.get(url).then(function (response) {
      setData(response.data);
    });
  }

  useEffect(() => {

    loadCountryData();

  }, [])

  return (
    <div>
      <MapHook data = { data } />
      <div className="Countries-Table-Wrapper">
        <CountriesTable data = { data } />
      </div>
    </div>
  );
};

export default MapParentComp;