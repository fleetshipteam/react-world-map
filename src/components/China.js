import React, { useEffect } from 'react';
import Button from 'react-bootstrap/Button';

import DetailsModal from './DetailsModal';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/chinaLow";
import am4themes_frozen from "@amcharts/amcharts4/themes/frozen";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_frozen);
am4core.useTheme(am4themes_animated);

const MapHook = () => {

  const [modalShow, setModalShow] = React.useState(false);
  const [selectedCountry, setSelectedCountry] = React.useState(null);
  const [countriesData, setCountriesData] = React.useState([]);

  var chinaPorts = [];

  useEffect(() => {

    // Create map instance
    var chart = am4core.create("chartdiv", am4maps.MapChart);

    chart.maxZoomLevel = 1;
    chart.seriesContainer.draggable = false;
    chart.seriesContainer.resizable = false;

    // Set map definition
    chart.geodata = am4geodata_worldLow;

    // Set projection
    chart.projection = new am4maps.projections.Miller();

    // Create map polygon series
    var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

    // Exclude Antartica
    polygonSeries.exclude = ["AQ"];

    // Make map load polygon (like country names) data from GeoJSON
    polygonSeries.useGeodata = true;

    console.log("polygonSeries.data = countriesData", countriesData);
    polygonSeries.data = countriesData;

    polygonSeries.tooltip.getFillFromObject = false;
    polygonSeries.tooltip.background.fill = am4core.color("#343440");
    polygonSeries.tooltip.background.stroke = am4core.color("#343440");

    // Configure series
    var polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    polygonTemplate.fill = am4core.color("#E8E8E8");
    polygonTemplate.propertyFields.fill = "fill";
    polygonTemplate.propertyFields.cursorOverStyle = "cursor";

    polygonTemplate.events.on("hit", function(ev) {
      var countryCode = ev.target.dataItem.dataContext.id;
      showModalView(countryCode);
    });

    // Create hover state and set alternative fill color
    var hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color("#8EC6D9"); 

    // Add image series
    var imageSeries = chart.series.push(new am4maps.MapImageSeries());
    imageSeries.mapImages.template.propertyFields.longitude = "longitude";
    imageSeries.mapImages.template.propertyFields.latitude = "latitude";
    imageSeries.mapImages.template.propertyFields.cursorOverStyle = "cursor";
    imageSeries.mapImages.template.tooltipText = "{title}";
    imageSeries.mapImages.template.events.on("hit", function(ev) {
      var countryCode = ev.target.dataItem.dataContext.title;
      showModalView(countryCode);
    });

    var circle = imageSeries.mapImages.template.createChild(am4core.Circle);
    circle.radius = 3;
    circle.propertyFields.fill = "color";

    var circle2 = imageSeries.mapImages.template.createChild(am4core.Circle);
    circle2.radius = 3;
    circle2.propertyFields.fill = "color";
    circle2.events.on("inited", function(event) {
      animateBullet(event.target);
    })

    function animateBullet(circle) {
        var animation = circle.animate([{ property: "scale", from: 1, to: 3 }, { property: "opacity", from: 1, to: 0 }], 1000, am4core.ease.circleOut);
        animation.events.on("animationended", function(event) {
          animateBullet(event.target.object);
        })
    }

    loadCountryData((d) => {

      console.log("polygonSeries.data = countriesData", d, d.length);
    
      if (d.length == 0) {
        return;
      }

      polygonSeries.data = d;

      var dotColor = am4core.color("#0091B8");

      imageSeries.data = [ {
        "title": "Dalian",
        "latitude": 38.916513,
        "longitude": 121.646827,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Dandong",
        "latitude": 40.109437,
        "longitude": 124.373860,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Fuzhou",
        "latitude": 26.062154,
        "longitude": 119.317172,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Huanghua",
        "latitude": 38.390300,
        "longitude": 117.456994,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Jiaxing",
        "latitude": 30.733630,
        "longitude": 120.786429,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Ningbo",
        "latitude": 29.875660,
        "longitude": 121.587059,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Ningde",
        "latitude": 26.653401,
        "longitude": 119.543266,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Putian",
        "latitude": 25.438914,
        "longitude": 119.023124,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Qinhuangdao",
        "latitude": 39.926785,
        "longitude": 119.596136,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Quanzhou",
        "latitude": 24.871667,
        "longitude": 118.657627,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Taizhou",
        "latitude": 28.656344,
        "longitude": 121.442773,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Tangshan",
        "latitude": 39.211976,
        "longitude": 118.349008,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Wenzhou",
        "latitude": 28.004542,
        "longitude": 120.709421,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Xiamen",
        "latitude": 24.471455,
        "longitude": 118.089746,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Yingkou",
        "latitude": 40.674368,
        "longitude": 122.214133,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Zhangzhou",
        "latitude": 24.501364,
        "longitude": 117.668035,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Zhoushan",
        "latitude": 30.010543,
        "longitude": 122.102734,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }, {
        "title": "Hong Kong",
        "latitude": 22.319294,
        "longitude": 114.150553,
        "color": dotColor,
        "cursor": am4core.MouseCursorStyle.pointer
      }];
    });

  }, [])

  const goBack = () => {
    window.location.href="/";
  };

  const loadCountryData = (callback) => {

    const axios = require('axios');
    // var url = "http://localhost:3000/dev/get-data-for-map-china";
    var url = "https://2v2zsydqb5.execute-api.us-east-2.amazonaws.com/dev/get-data-for-map-china";

    if (process.env.REACT_APP_BUILD_MODE === 'prod') {
      url = "https://wpyc9euq91.execute-api.us-east-2.amazonaws.com/prod/get-data-for-map-china";
    }
  
    axios.get(url).then(function (response) {

      var dataArray = [];

      if (response.data) {
        dataArray = JSON.parse(response.data);
      }

      chinaPorts = dataArray;

      var result = [];

      dataArray.forEach(element => {
        let item = {
          "id": element.code,
          "text": element.text,
          "title": element.title,
          "fill": am4core.color("#0091B8"),
          "cursor": am4core.MouseCursorStyle.pointer
        }
        result.push(item);
      });

      setCountriesData(result);
      callback(result);

    }).catch(function (error) {
      console.log('## Error:', error);
    });
  }

  return (
    <div style={{ width: "80%" }}>
      <div class="container">
        <div class="row">
          <div class="col-sm China-Back-Button-Wrapper">
            <Button className="China-Back-Button" variant="light" onClick={ goBack } >Back</Button>
          </div>
          <div class="col-sm">
            <div className='Map-Title-Wrapper-China'>
              <span className='Map-Subtitle'>China</span>
            </div>
          </div>
          <div class="col-sm">

          </div>
        </div>
      </div>
      
      <div id="chartdiv" style={{ width: "100%", height: "620px" }}></div>
      <DetailsModal show={modalShow} country={selectedCountry} onHide={() => setModalShow(false)} />
    </div>
  )


  function showModalView(countryCode) {
    debugger;
    var country = getCountryByCode(countryCode);
    if (country) {
      setSelectedCountry(country);
      setModalShow(true);
      return
    }

    var place = getPlaceByTitle(countryCode);
    if (place) {
      setSelectedCountry(place);
      setModalShow(true);
    }
  }

  function getCountryByCode(code) {

    const result = chinaPorts.find(function(country) {
      return country.code === code;
    });

    return result;
  }

  function getPlaceByTitle(title) {

    const result = chinaPorts.find(function(port) {

      var t = cleaned(title);
      var pcode = cleaned(port.code);
      var ptitle = cleaned(port.title);

      return pcode === t || ptitle === t;
      
    });

    return result;
  }

  function cleaned(string) {

    if (string) {
      return string.replace("Ports", "").replace("Port", "").trim();
    }

    return "";
  }
}

export default MapHook;